---
title: STACL - Simultaneous translation with implicit anticipation and controllable latency using prefix-to-prefix framework
author: Jonne Sälevä
date: 04/21/2020
---

# Introduction

::: incremental

- In this talk, we will go over the STACL paper by @ma2018stacl dealing with simultaneous translation
- Key contributions: 
    - new incremental attention mechanism using Transformers
    - attending to prefixes with controllable latency
    - implicitly learns to anticipate future words

- Find slides on Gitlab: [https://gitlab.com/jonnesaleva/stacl](https://gitlab.com/jonnesaleva/stacl)

:::

# Simultaneous translation

::: incremental

- Again, this paper deals with simulatenous translation, i.e. translating into the target language before all source language words have been consumed
- Characterized by a certain **latency**, even with humans, and the need to **anticipate** future words that haven't been seen yet
- "Notoriously difficult for machines" especially because of word order differences
    - e.g. German embedded clause word order is tricky

:::

# Background: Full-sentence NMT formalism

- Input: sequence of words (1-hot vectors) in one language
$$
\mathbf{x} = (\mathbf{x}_1, \dots , \mathbf{x}_{T_x})
$$

- An **encoder network** $f$ transforms the *d*-dimensional input vectors into a hidden vector sequence
$$
\mathbf{h} = (\mathbf{h}_1, \dots , \mathbf{h}_{T_x}) = f(\mathbf{x})
$$

- This encoder can be implemented as a RNN or Transformer. Here the authors present only Transformer "due to space constraints".

- Output: sequence of words in another language
$$
\mathbf{y} = (\mathbf{y}_1, \dots , \mathbf{y}_{T_y})
$$

# Background: Full-sentence NMT formalism

- The output words are predicted by a **decoder** network given the source sequence hidden representation $\mathbf{h}$.

- The decoder stops generating when it generates a special end-of-sentence symbol

- We wish to discriminatively model the outputs conditional on the inputs

$$
p(\mathbf{y} | \mathbf{x}) = \prod_{t=1}^{|\mathbf{y}|} p(y_t | \mathbf{x}, \mathbf{y}_{<t})
$$

- Overall loss function

$$
l(D) = -\sum_{(\mathbf{x}, \mathbf{y}^{*}) \in D} \log{p(\mathbf{y^{*}}|\mathbf{x})} 
$$

# Prefix-to-Prefix Training

::: incremental

- In full-sentence translation every $y_t$ is predicted conditional on the entire input sequence
- Obviously, in simulatenous translation this is not possible, so we need to translate concurrently with the source sentence
- Let's now take a look at the prefix-to-prefix architecture

:::

# Prefix-to-Prefix Training

- Let's first define a helpful quantity $g(t)$, describing the number of source words "consumed" when we generate $y_t$.
- Thus the decoding probability becomes

$$
p(\mathbf{y} | \mathbf{x}) = \prod_{t=1}^{|\mathbf{y}|} p(y_t | \mathbf{x}_{<g(t)}, \mathbf{y}_{< t})
$$

- Note that if we set $g(t) = |\mathbf{x}|$ we recover traditional full-sentence translation, and setting $g(t) = 0$ gives us "oracle" based machine translation.

# Prefix-to-Prefix Training

- Let's also define $\tau_g(|\mathbf{x}|)$ which describes the "cut-off" index $t$ at which point all source words have been consumed

$$
\tau_g(|\mathbf{x}|) = \min \{t | g(t) = |\mathbf{x}|\}
$$

- In P2P training, we *train* the model to predict using the source prefixes by modifying the loss function using $g(t)$. 

- This is in contrast to previous literature, where training is done with full sentences and only decoding uses P2P

# Prefix-to-Prefix Training

![Cut-off point](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig1.png)

# Wait-k policy

- Simple example of P2P framework: wait-k policy
- Idea: first wait $k$ words, then decode concurrently with rest of the source sentence
- Thus: output always lagging exactly $k$ words behind
- Then, $g(t)$ becomes

$$
g_{\text{wait-k}}(t) = \min \{k+t-1, |\mathbf{x}|\}
$$

# Wait-k policy

- The cut-off point $\tau_{g_{\text{wait-k}}}(|\mathbf{x}|)$ is then: 

$$\tau_{g_{\text{wait-k}}}(|\mathbf{x}|) = |\mathbf{x}| - k + 1$$

- Part after the cut-off known as **tail** and translated according to full-sentence NMT
    - "Tail beam search" vs. greedy decoding for earlier $y_t$

# Wait-k policy

- Note: wait-k used also in training, and proves to be very important when k is small.
- As $k$ increases, authors observe that genuine wait-k and test-time-only wait-k approach the full sentence baseline

![Effect of using train-time wait-k](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig2.png)

# Recap: Transformer

- Recall that in the regular transformer, the encoder creates $\mathbf{z} = (z_1, \dots, z_n)$, with each $z_i \in \mathbb{R}^d$ as:

$$
z_i = \sum_{j=1}^n \alpha_{ij} P_{W_V}(x_j)
$$

- The attention weights $\alpha_{ij}$ are simply normalized energies $e_{ij}$ which are calculated as

$$
e_{ij} = \frac{P_{W_Q}(x_i)P_{W_K}(x_j)^\top}{\sqrt{d_x}}
$$

- Since decoding is incremental, we set $\alpha_{ij} = 0$ if $j > i$

- On the decoder side, $\mathbf{y}^*$ undergoes similar self-attention and becomes $c = (c_1, \dots, c_m)$.

- Finally target-to-source attention is applied

$$
c'_i = \sum_{j=1}^n \beta_{ij} P_{W_V}(h_j)
$$

# The Simultaneous Transformer

- In the simultaneous translation case, we need to feed in words incrementally to the encoder
- Here we simulate that by computing the self-attention weights only for cases where $i$ and $j$ are less than $g(t)$, and otherwise set them to 0
- The new hidden state sequence $\mathbf{z^{(t)}} = (z^{(t)}_1, \dots, z^{(t)}_n)$ is then generated with

$$
z^{(t)}_i = \sum_{j=1}^n \alpha^{(t)}_{ij} P_{W_V}(x_j)
$$

- Upon receiving a new source word, a new hidden representation must be computed 

# Measuring latency: Consecutive Wait

- Besides BLEU / translation quality, it is also important to measure latency when dealing with simultaneous translation
- An existing metric for this is **consecutive wait** which measures the number of source words waited between decoding steps:

$$
\text{CW}_{g}(t) = g(t) - g(t-1)
$$

- Thus

$$
\text{CW}_g(\mathbf{x}, \mathbf{y}) = \frac{\sum_{t=1}^{|\mathbf{y}|} \text{CW}_g (t) }{\sum_{t=1}^{|\mathbf{y}|} \mathbb{I}[\text{CW}_g(t) > 0]} = \frac{|\mathbf{x}|}{\sum_{t=1}^{|\mathbf{y}|} \mathbb{I}[\text{CW}_g(t) > 0]}
$$

- CW measures the average source segment length. Drawback: "CW is a local latency measurement which is insensitive to the actual lagging behind".

# Measuring latency: Average Proportion

- Another latency metric, measures the proprtion of the area above a policy path

![Policy path](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig1.png)

# Measuring latency: Average Proportion

- AP is defined as

$$
\text{AP}_g(\mathbf{x}, \mathbf{y}) = (|\mathbf{x}||\mathbf{y}|)^{-1}\sum_{t=1}^{|\mathbf{y}|} g(t)
$$

- Major flaws
    1) Sensitive to input length
    2) "Being a percentage it is not obvious to the user the actual delays in number of words"

# Measuring latency: Average Lagging

- Inspired by "lagging behind the ideal policy", the authors define a new metric called **"average lagging"** as:

$$
\text{AL}_g(\mathbf{x}, \mathbf{y}) = (\tau_g(|\mathbf{x}|))^{-1}\sum_{t=1}^{\tau_g(|\mathbf{x}|)} g(t) - (t-1)
$$

- Note that when we use *wait-k* as our policy, AL is equal to $k$.

- Since there is often a difference between source and target lengths, we define $r = \frac{|\mathbf{y}|}{|\mathbf{x}|}$ and redefine AL as

$$
\text{AL}_g(\mathbf{x}, \mathbf{y}) = (\tau_g(|\mathbf{x}|))^{-1}\sum_{t=1}^{\tau_g(|\mathbf{x}|)} g(t) - \frac{(t-1)}{r}
$$

- With this "catchup" alteration, the AL is roughly equal to $k$

# Measuring latency: Average Lagging

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig4.png)

# Data & Experiments

- Four different simulatenous translation directions
    - German <-> English
    - Chinese <-> English

- Training data: parallel corpora from WMT15 (German-English) and NIST (Chinese-English)

- Apply BPE as preprocessing to reduce vocab size

- Report 4-reference BLEU when translating from Chinese to English, and use 2nd reference as the source text, otherwise use 1-reference BLEU scores

- See full info in Section 6.1 of the paper

# Quality & Latency of Wait-k

- Overall it seems like training on a slightly larger k than you decode with is optimal

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/table1.png)

# Quality & Latency of Wait-k

- When plotting translation quality vs latency, we observe two things as $k$ increases:
    - a) wait-$k$ improves in BLEU and worsens in latency
    - b) the gap between test-time wait-$k$ and regular wait-$k$ shrinks

- Eventually, both approach the full sentence baseline

- Plot on next slide :)

# Quality & Latency of Wait-k

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig5.png)

# Quality & Latency of Wait-k
![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig6.png)

# Quality & Latency of Wait-k
![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig7.png)

# Quality & Latency of Wait-k

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig8.png)

# Human evaluation on anticipation

- The authors also performed human evaluations of anticipation using bilingual speakers of English and German/Chinese
- Evaluations on anticipation rates and accuracy based on 100 examples from the development set
- Not entirely clear to me *what* they were evaluating?
    - Whether an anticipation was present in the translation and whether it was correct?

# Human evaluation on anticipation

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/table2.png)

# Human evaluation on anticipation

- Overall ranking of the 4 pairs:

```
en-zh > de-en > zh-en > en-de
```

- English-Chinese is particularly difficult due to reorderings of phrases such as "in recent years"
- Authors explain English-German difficulty with the challenges of simultaneous translation from SOV to SVO, which includes "predicting the verb".

# Qualitative examples

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig9.png)

# Qualitative examples

![](/home/think/Documents/coding/mt-seminar-projects/p4-stacl/img/fig13.png)

# References #

