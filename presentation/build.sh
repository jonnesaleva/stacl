INPUT_FILE="presentation.md"
OUTPUT_FILE="stacl-presentation.pdf"
BIBLIOGRAPHY_FILE="_pres.bib"
PDF_VIEWER="zathura"

pandoc \
    -t beamer \
    --filter pandoc-citeproc \
    --bibliography $BIBLIOGRAPHY_FILE \
    -o $OUTPUT_FILE $INPUT_FILE

$PDF_VIEWER $OUTPUT_FILE
