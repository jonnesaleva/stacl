# STACL: Simultaneous translation with implicit anticipation and controllable latency using prefix-to-prefix framework

See `./presentation/` for details. :)

## References

Ma, Mingbo, et al. "STACL: Simultaneous translation with implicit anticipation and controllable latency using prefix-to-prefix framework." arXiv preprint arXiv:1810.08398 (2018).
